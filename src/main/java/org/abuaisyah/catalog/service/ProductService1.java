package org.abuaisyah.catalog.service;

import org.abuaisyah.catalog.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductService1 extends JpaRepository<Product, String>, ProductService {
}
