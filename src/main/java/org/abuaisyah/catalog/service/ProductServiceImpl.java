package org.abuaisyah.catalog.service;

import org.abuaisyah.catalog.entity.Product;
import org.hibernate.SessionFactory;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class ProductServiceImpl implements ProductService {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List pencarianCustom() {
        return entityManager.createQuery("select new map(o.id as id) from Product o").getResultList();
    }
}
