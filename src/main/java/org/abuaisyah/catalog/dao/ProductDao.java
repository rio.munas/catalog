package org.abuaisyah.catalog.dao;

import org.abuaisyah.catalog.entity.Product;
import org.abuaisyah.catalog.service.ProductService;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductDao extends PagingAndSortingRepository<Product, String> {
}
