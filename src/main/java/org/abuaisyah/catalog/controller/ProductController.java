package org.abuaisyah.catalog.controller;

import org.abuaisyah.catalog.dao.ProductDao;
import org.abuaisyah.catalog.entity.Product;
import org.abuaisyah.catalog.service.ProductService;
import org.abuaisyah.catalog.service.ProductService1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ProductController {
    @Autowired private ProductService1 service1;

    @RequestMapping("/")
    public List<Product> getAll() {
        return service1.pencarianCustom();
    }
}
